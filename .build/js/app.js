'use strict';



define('app', ['config'], function(config) {
  require(['socket.io'], function(io) {
    var app = {
      initialize: function() {
        var socket = io.connect('http://localhost');
        socket.on('news', function(data) {
          console.log(data);
          socket.emit('my other event', {
            my: 'data'
          });
        });
      }
    };
    alert('hi');
    app.initialize();
  });
});