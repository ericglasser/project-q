'use strict';


requirejs.config({
	paths: {
		'socket.io': '/socket.io/socket.io'
	},
	shim: {
		// Export io object: https://gist.github.com/guerrerocarlos/3651490
		'socket.io': {
			exports: 'io'
		}
	}
});